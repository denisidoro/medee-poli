About this project
---------------

Medee is a framework to support the situational method composition to develop multiagent systems (MAS). Such a composition relies on MAS method fragments that are tailored to specific project situation. This project intends to build a plugin to be integrated to the Medee Framework in order to assist the fragments selection and facilitate the method composition.

Important link
---------------

  - [Medee thesis] [thesis]

  [thesis]: http://www.teses.usp.br/teses/disponiveis/3/3141/tde-05032012-162517/pt-br.php