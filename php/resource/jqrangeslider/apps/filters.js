  
var today = new Date();

$("#popularitySlider").rangeSlider({
 
  bounds: {
    min: 0,
    max: 60000
  },
  
  defaultValues: {
    min: 0,
    max: 60000
  },
  
  range: {
    min: 1000
  },
  
  valueLabels: "change",
  
  formatter :function(val) {
  
    val = Math.round(val);
          
    if (val >= 60000)
      val += "+";
      
    return val;
    
  }

});

$("#durationSlider").rangeSlider({
 
  bounds: {
    min: 50,
    max: 180
  },
  
  defaultValues: {
    min: 50,
    max: 180
  },
  
  range: {
    min: 20
  },
  
  valueLabels: "change",
  
  formatter :function(val) {
  
    roundVal = Math.round(val);
    var hours = Math.floor(roundVal/60); 
    var minutes = roundVal%60;

    var string = hours + "h";
    if (minutes > 0)
      string += minutes;
      
    if (val <= 50)
      string += "-";
    else if (val >= 180)
      string += "+";
      
    return string;
    
  }

});
 
$("#scoreSlider").rangeSlider({
 
  bounds: {
    min: 0,
    max: 100
  },
  
  defaultValues: {
    min: 70,
    max: 100
  },
  
  range: {
    min: 10
  },
  
  valueLabels: "change"

});
 
$("#dateSlider").dateRangeSlider({
 
  bounds: {
    min: new Date(1993, 0, 1),
    max: today
  },
  
  defaultValues: {
    min: new Date(1998, 0, 1),
    max: today
  },
  
  range: {
    min: {days: 15}
  },
  
  valueLabels: "change",
  
  formatter :function(val) {
  
    var days = val.getDate(),
    month = val.getMonth() + 1,
    year = val.getFullYear();
    
    var string = month + "/" + year;
    
    if (days == 1 && month == 1 && year == 1993)
      string += "-";
    else if (days == today.getDate() && month == today.getMonth() + 1 && year == today.getFullYear())
      string += "+";    
    
    return string;
    
  }

});
