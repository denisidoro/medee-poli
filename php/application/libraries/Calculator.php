<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calculator extends Reader {

	public function get_weights($indexes, $weights)
	{

		$this->setup_initial_weights($weights);
		$this->consider_selected_factors($indexes, $weights);

		return $this->weighted;

	}

	public function setup_initial_weights($weights)
	{

		$factors = $this->get_factors();
		$indexes = $this->get_factor_index_array($factors);

		foreach ($indexes as $index)
		{

			$fragments = $this->get_fragments_from_factors(array($index), $factors);

			foreach ($fragments as $f)
			{
				if (isset($this->weighted[$f]))
					$this->weighted[$f] += $weights[$index[0]][$index[1]]; 
				else
					$this->weighted[$f] = 0;
			}

		}

		return $this->weighted;

	}

	public function consider_selected_factors($indexes, $factors)
	{

		foreach ($indexes as $index)
		{

			$fragments = $this->get_fragments_from_factors(array($index), $factors);

			foreach ($fragments as $f)
				$this->weighted[$f] += 10; 

		}

		return $this->weighted;

	}

}