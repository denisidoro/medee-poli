<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Epfparser {

	var $elements_array = array();
	var $guideline_categories = array();
	var $category_fragments = array();
	var $fragment_codes = array();
	var $category_folders = array();

	public function get_elements_array($guidelines = FALSE)
	{

		$source = MEDEE_DIRECTORY . 'c_medee_project_factors_taxonomy/plugin.xmi';
		$xml = simplexml_load_file($source);

		$contentElements = $xml->xpath('//contentElements');

		$myElem = array();

		foreach ($contentElements as $elem)
			$myElem[] = array(
				'presentationName' => trim((string)$elem['presentationName']),
				'name' => (string)$elem['name'],
				'guid' => (string)$elem['guid'],
				'categorizedElements' => (string)$elem['categorizedElements']
			);

		unset($contentElements);

		foreach ($myElem as $elem)
			if (!empty($elem['categorizedElements']) && !empty($elem['presentationName']) && stripos( $elem['presentationName'], 'taxonomy') == FALSE)
				$elements_array[] = $elem;

		foreach ($myElem as $elem)
			if (empty($elem['categorizedElements']) and !empty($elem['guid']))
				for ($i = 0; $i < count($elements_array); $i++)
					if (stripos($elements_array[$i]['categorizedElements'], $elem['guid']) !== FALSE)
					{
						$elements_array[$i]['subfactors'][] = $elem;
						break;
					}

		return ($guidelines) 
			? $this->fill_array_with_guidelines($elements_array) 
			: $elements_array;

	}

	private function fill_array_with_guidelines($elements_array)
	{

		for ($i = 0; $i < count($elements_array); $i++)
				for ($j = 0; $j < count($elements_array[$i]['subfactors']); $j++)
					if (!empty($elements_array[$i]['subfactors'][$j]['name']))
						$elements_array[$i]['subfactors'][$j]['guidelines'] = $this->get_factor_guidelines($elements_array[$i]['subfactors'][$j]['name']);

		return $elements_array;		

	}

	public function get_array_with_fragments()
	{

		if (empty($this->elements_array))
			$this->elements_array = $this->get_elements_array(TRUE);

		$info = array();

		foreach ($this->elements_array as $factor)
			foreach ($factor['subfactors'] as $subfactor)
				if (isset($subfactor['guidelines']))
					foreach ($subfactor['guidelines'] as $guideline)
					{

						if (!isset($guideline['name']) || is_numeric($guideline['name']))
							break;

						$categories = $this->get_guidelines_categories(urlencode($guideline['name']));

						if (!isset($this->guideline_categories[$guideline['name']]))
						{

							$this->guideline_categories[$guideline['name']] = array_merge(
								$guideline, 
								array('categories' => $categories)
							);

						}

						unset($categories);

					}

		foreach ($this->guideline_categories as $guideline)
			if (is_array($guideline['categories']))
				foreach ($guideline['categories'] as $category)
					if (isset($category['name']))
						$this->category_fragments[$category['name']] = array_merge(
							$category,
							array('fragments' => $this->get_category_fragments($category['name']))
						);

		return array(
			$this->elements_array,
			$this->guideline_categories,
			$this->category_fragments,
			$this->get_mmf_names(),
			$this->fragment_codes
		);

	}

	public function get_factor_guidelines($name)
	{

		$source = MEDEE_DIRECTORY . "c_medee_project_factors_taxonomy/customcategories/{$name}.xmi";
		$xml = simplexml_load_file($source);

		$consideration = $xml->keyConsiderations;

		$matches = $this->extract_links($consideration);
		//print_r($matches);

		$guidelines = array();

		foreach ($matches as $match)
		{

			$dirs = explode('/', $match[1]);

			$guidelines[] = array(
				'presentationName' => $match[2], 
				'name' => substr(end($dirs), 0, strrpos(end($dirs), '_')),
				'param' => $name
			);

			/*
			if (stripos($match[3], "Instable requirements") !== false)
			{
				echo "DEBUG INSTABLE";
				print_r($matches);
				exit();
			}
			*/

		}
		
		return $guidelines;

	}

	public function get_guidelines_categories($guideline_name)
	{

		$source = MEDEE_GUIDELINES_DIRECTORY . "{$guideline_name}.xmi";

		if (file_exists(str_replace(SERVER_LOCATION, '', $source)))
			$xml = simplexml_load_file($source);
		else
		{	
			log_message('debug', "Não foi possível achar o seguinte arquivo: $source");
			return -1;
		}

		$mainDescriptions = $xml->xpath('//mainDescription');

		$categories = array();
		$links = $this->extract_links($mainDescriptions[0]);


		foreach ($links as $link)
		{

			$temp = explode('/', $link[1]);
			
			$categories[] = array(
				'name' => substr(end($temp), 0, strrpos(end($temp), '_')),
				'presentationName' => trim($link[2])
			);

		}

		/*
		if (stripos($link[3], "experimental development") !== false)
		{
			echo "debug 2:";
			print_r($links);
		}
		*/

		return $categories;

	}

	public function get_category_fragments($category_name, $translate = TRUE)
	{

		$source = MEDEE_DIRECTORY . 'b_medee_semiotic_taxonomy/plugin.xmi';
		$xml = simplexml_load_file($source);

		$category_name = str_replace(" ", "_", $category_name);

		$fragments = array();
		$fragments_info = $xml->xpath("//contentElements[@name='{$category_name}']/categorizedElements"); 

		//echo "$category_name<br>" . count($fragments_info) . "<br><br>";

		if (empty($fragments_info))
		{

			$category = $xml->xpath("//contentElements[@name='{$category_name}']");

			if (!empty($category))
			{

				$elems = explode(" ", (string)$category[0]['categorizedElements']);

				foreach ($elems as $elem)
				{
					$fragments_info = $xml->xpath("//contentElements[@guid='{$elem}']/categorizedElements");
					$fragments = $this->iterate_categories($fragments_info, $fragments, $translate);
				}

			}

		}

		else
			$fragments = $this->iterate_categories($fragments_info, $fragments, $translate);
		
		return $fragments;

	}

	private function iterate_categories($fragments_info, $fragments, $translate = TRUE)
	{

		foreach ($fragments_info as $fragment)
		{

			$href = (string)$fragment['href'];
			$regexp = "^uma://(?<code>[_\w-]*)#[_\w-]*";
			preg_match_all("|$regexp|siU", $href, $matches, PREG_SET_ORDER);

			$fragments[] = ($translate) 
				? $this->translate_fragment_code($matches[0]['code'])
				: $matches[0]['code'];

			unset($matches);

		}

		return $fragments;

	}

	private function translate_fragment_code($code)
	{

		if (array_key_exists($code, $this->fragment_codes))
			return $this->fragment_codes[$code];

		if (empty($this->category_folders))
			$this->category_folders = get_category_folders();

		for ($attempt = 0; $attempt < count($this->category_folders); $attempt++)
		{

			$source = MEDEE_DIRECTORY . $this->category_folders[$attempt] . '/plugin.xmi';
			$xml = simplexml_load_file($source);

			$temp = $xml->xpath("//resourceDescriptors[@id='{$code}']");

			if (isset($temp[0]))
			{

				$dirs = explode('/', (string)$temp[0]['uri']);
				$this->fragment_codes[$code] = $dirs[1];
				return $dirs[1];

			}

		}

		return NULL;

	}

	private function extract_links($source)
	{

		$source = str_replace(array("\n", " _", "_ "), array("", "_", "_"), $source);
		
		//$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
		$regexp = '<a\s.*href="([\w\s\.\d_\/]*)"[^>]*>(.*)</a>';
		
		preg_match_all("|$regexp|siU", $source, $matches, PREG_SET_ORDER);
		return $matches;

	}

	public function get_mmf_names() {

		$mmf_names = array();

		$folder = str_replace(SERVER_LOCATION, './', MEDEE_DIRECTORY);
		$xmis = glob("{$folder}*/capabilitypatterns/mmf*/model.xmi");

		foreach ($xmis as $xmi) {

			$regex = str_replace(SERVER_LOCATION, '', MEDEE_DIRECTORY) . "([^/]*)/capabilitypatterns/([^/]*)";
			preg_match("|{$regex}|i", $xmi, $matches);

			$source = MEDEE_DIRECTORY . $matches[1] . "/capabilitypatterns/" . $matches[2] . "/model.xmi";
			$xml = simplexml_load_file($source);

			$result = $xml->xpath("//process[@name='" . $matches[2] . "']"); 

			if (!empty($result))
				$mmf_names[$matches[2]] = (string)$result[0]['presentationName'];

		}

		return $mmf_names;

	}

}