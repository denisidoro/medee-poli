<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reader {

	var $weighted = array();

	/**
	 * 
	 * @param $indexes array of selected indexes
	 * @param $factors list of factors
	 * @return $fragments all fragments
	 */
	public function get_fragments_from_factors($indexes, $factors = array())
	{

		if (empty($factors))
			$factors = $this->get_factors();

		$guidelines = $this->get_guidelines($indexes, $factors);
		$categories = $this->get_categories($guidelines, $factors);
		$fragments = $this->get_fragments($categories, $factors);

		return $fragments;

	}

	public function get_factor_index_array($factors)
	{

		$indexes = array();

		for ($i = 0; $i < count($factors); $i++)
			for ($j = 1; $j < count($factors[$i]); $j++)
				$indexes[] = array($i, $j);

		return $indexes;

	}

	public function get_factors()
	{

		$source = MAIN_XML_LOCATION . "factors.xml";
		$xml = simplexml_load_file($source);

		$factors = array();
		$groups = $xml->xpath('factorgroup');

		foreach ($groups as $g)
		{

			$temp[] = array(
				'name' => (string)$g['name'],
				'presentationName' => (string)$g['presentationName']
			);
			
			$factor = $xml->xpath("factorgroup[@name='" . (string)$g['name'] . "']/factor");	

			foreach ($factor as $f)
				$temp[] = array(
					'name' => (string)$f['name'],
					'presentationName' => (string)$f['presentationName']
				);

			$factors[] = $temp;
			unset($temp);

		}

		return $factors;

	}

	public function get_guidelines($indexes, $factors = array())
	{

		if (empty($factors))
			$factors = $this->get_factors();

		$factors = $this->get_factors();

		$source = MAIN_XML_LOCATION . "factors.xml";
		$xml = simplexml_load_file($source);

		$guidelines = array();

		foreach ($indexes as $i)
		{

			$guid = $xml->xpath("//factor[@name='" . $factors[$i[0]][$i[1]]['name'] . "']//guideline");

			foreach ($guid as $g)
				if (!in_array((string)$g['name'], $guidelines))
					$guidelines[] = (string)$g['name'];

		}

		return $guidelines;

	}

	public function get_categories($guidelines, $factors = array())
	{

		if (empty($factors))
			$factors = $this->get_factors();

		$factors = $this->get_factors();

		$source = MAIN_XML_LOCATION . "guidelines.xml";
		$xml = simplexml_load_file($source);

		$categories = array();

		foreach ($guidelines as $guid)
		{

			$cat = $xml->xpath("//guideline[@name='{$guid}']//category");

			foreach ($cat as $c)
				if (!in_array((string)$c['name'], $categories))
					$categories[] = (string)$c['name'];

		}

		return $categories;

	}

	public function get_fragments($categories, $factors = array())
	{		

		if (empty($factors))
			$factors = $this->get_factors();

		$source = MAIN_XML_LOCATION . "categories.xml";
		$xml = simplexml_load_file($source);

		$fragments = array();

		foreach ($categories as $cat)
		{

			$frag = $xml->xpath("//category[@name='{$cat}']//fragment");

			foreach ($frag as $f)
				if (!in_array((string)$f['name'], $fragments))
					$fragments[] = (string)$f['name'];	

		}

		return $fragments;
		
	}

	public function get_discipline($name)
	{		

		$source = MAIN_XML_LOCATION . "categories.xml";
		$xml = simplexml_load_file($source);

		$parents = array();
		$searchFor = array('requirements', 'analysis', 'design', 'implementation', 'test');
		array_walk($searchFor, create_function('&$v, $k', '$v .= "_discipline";'));

		if ($result = $xml->xpath("//fragment[@name='{$name}']/ancestor::*"))
			foreach ($result as $r)
			{
				$catName = (string)$r['name'];
				foreach ($searchFor as $s)
					if ($catName == $s)
						$parents[] = $catName;
			}
			
		return $parents;

	}

	public function get_component($name)
	{		

		$source = MAIN_XML_LOCATION . "categories.xml";
		$xml = simplexml_load_file($source);

		$parents = array();
		$searchFor = array('agent', 'environment', 'interaction', 'organization');
		array_walk($searchFor, create_function('&$v, $k', '$v .= "_component";'));

		if ($result = $xml->xpath("//fragment[@name='{$name}']/ancestor::*"))
			foreach ($result as $r)
			{
				$catName = (string)$r['name'];
				foreach ($searchFor as $s)
					if ($catName == $s)
						$parents[] = $catName;
			}
			
		return $parents;

	}

}
