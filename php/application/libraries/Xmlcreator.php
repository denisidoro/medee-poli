<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xmlcreator extends Epfparser {

	public function generate_xml($arrays, $save_to_file = TRUE)
	{

		return array(
			$this->generate_xml_guidelines($arrays[0], $save_to_file),
			$this->generate_xml_categories($arrays[1], $save_to_file),
			$this->generate_xml_fragments($arrays[2], $arrays[3], $save_to_file)
		);

	}

	private function generate_xml_guidelines($array, $save_to_file)
	{		

		$xml = array();

		foreach ($array as $factors)
		{

			$factor_array = array();

			foreach ($factors['subfactors'] as $factor)
			{

				$guideline_array = array();

				foreach ($factor['guidelines'] as $guideline)
				{

					$guideline_array[] = array(
						'@attributes' => array(
							'name' => $guideline['name'],
							'presentationName' => $guideline['presentationName']
						)
					);

				}

				$factor_array[] = array(
					'@attributes' => array(
						'name' => $factor['name'],
						'presentationName' => $factor['presentationName']
					),
					'guideline' => $guideline_array
				);

			}

			$xml[] = array(
				'@attributes' => array(
					'name' => $factors['name'],
					'presentationName' => $factors['presentationName']
				),
				'factor' => $factor_array
			);

		}
		
		return $this->save_xml($xml, 'factorgroup', 'factors');

	}

	private function generate_xml_categories($array, $save_to_file)
	{

		$xml = array();

		foreach ($array as $guideline)
		{

			$category_array = array();

			if (is_array($guideline['categories']))
			{

				foreach ($guideline['categories'] as $category)
				{

					$category_array[] = array(
						'@attributes' => array(
							'name' => $category['name'],
							'presentationName' => $category['presentationName']
						)						
					);

				}

				$xml[] = array(
					'@attributes' => array(
						'name' => $guideline['name'],
						'presentationName' => $guideline['presentationName']
					),
					'category' => $category_array
				);

			}

		}
		
		return $this->save_xml($xml, 'guideline', 'guidelines');

	}

	private function generate_xml_fragments($array, $names, $save_to_file)
	{

		$xml = array();

		foreach ($array as $category)
		{

			$fragment_array = array();

			foreach ($category['fragments'] as $fragment)
			{

				$fragment_array[] = array(
					'@attributes' => array(
						'name' => $fragment,
						'presentationName' => $names[$fragment]
					)						
				);

			}

			$xml[] = array(
				'@attributes' => array(
					'name' => $category['name'],
					'presentationName' => $category['presentationName']
				),
				'fragment' => $fragment_array
			);

		}
		
		return $this->save_xml($xml, 'category', 'categories');

	}

	private function save_xml($xml, $name, $filename)
	{

		$xml_obj = Array2XML::createXML('info', array($name => $xml));
		$xml_obj = $xml_obj->saveXML();

		$handle = fopen(MAIN_XML_LOCATION . "{$filename}.xml", "w");
		fwrite($handle, $xml_obj);
		fclose($handle);

		return array($xml, $xml_obj);

	}

}