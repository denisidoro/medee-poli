<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Graphics extends CI_Controller {

	public function div() {

		$data = array(
			'view' => 'test/div',
			'title' => 'test',
			'content' => array()
		);

		$this->template->default_write($data);

	}

}