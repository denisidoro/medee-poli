<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Docs extends CI_Controller {

	public function index()
	{
		header('Location: ' . SERVER_LOCATION . 'explorer.php?dir=documentation');
	}

	public function custom()
	{

		echo "<link rel='stylesheet' href='" . SERVER_LOCATION . "resource/css/main.css' type='text/css'/>";
		$this->listFolderFiles('documentation');

	}

	private function listFolderFiles($dir, $exclude = array())
	{ 
	    $ffs = scandir($dir); 
	    echo '<ul class="ulli">'; 
	    foreach($ffs as $ff){ 
	        if(is_array($exclude) and !in_array($ff,$exclude)){ 
	            if($ff != '.' && $ff != '..'){ 
	            if(!is_dir($dir.'/'.$ff)){ 
	            echo '<li><a href="'.SERVER_LOCATION.ltrim($dir.'/'.$ff,'./').'">'.$ff.'</a>'; 
	            } else { 
	            echo '<li>'.$ff;    
	            } 
	            if(is_dir($dir.'/'.$ff)) listFolderFiles($dir.'/'.$ff,$exclude); 
	            echo '</li>'; 
	            } 
	        } 
	    } 
	    echo '</ul>'; 
	} 

}