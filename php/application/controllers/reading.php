<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reading extends CI_Controller {

	public function old()
	{

		$this->load->library('reader');
		$this->load->helper('form');
		
		$menu_elements = $this->reader->get_factors();

		$data = array('menu_elements' => $menu_elements);
		$this->load->view('main', $data);

	}

	public function temp() {

		$this->load->library('reader');
		$this->load->helper('form');

		$data = array(
			'view' => 'result',
			'title' => 'Medee',
			'content' => array(
				'menu_elements' => $this->reader->get_factors()
			)
		);

		$this->template->default_write($data);

	}

	public function index() {

		$this->load->library('reader');
		$this->load->helper('form');

		$data = array(
			'view' => 'main_slider',
			'title' => 'Medee',
			'content' => array(
				'menu_elements' => $this->reader->get_factors()
			)
		);

		$this->template->default_write($data);

	}

	public function get_result()
	{

		$this->load->library(array('epfparser', 'reader', 'calculator'));

		$names = $this->epfparser->get_mmf_names();

		$indexes = $this->input->get('indexes', array());
		$weights = $this->input->get('weights', array());
		$method = $this->input->get('method', 0);

		switch ($method)
		{
			case 0:
				$result = $this->calculator->get_weights($indexes, $weights);
				break;
			case 1:
				$result = $this->calculator->setup_initial_weights($weights);
				break;
			case 2:
				$result = $this->calculator->get_fragments_from_factors($indexes);
				break;
		}

		$detailed = array();

		foreach ($result as $r => $v) {

			$discipline = $this->reader->get_discipline($r);
			$component = $this->reader->get_component($r);

			$name = (isset($names[$r])) ? $names[$r] : $r;

			$detailed[] = array(
				$r,
				$v,
				exst($discipline[0]),
				exst($component[0]),
				$name
			);

		}

		function cmp($a, $b) {
			return $b[1] - $a[1];
		}

		usort($detailed, "cmp");
		echo json_encode($detailed);

	}

	public function get_discipline()
	{

		$this->load->library('reader');
		$mmf = $this->input->get('mmf', '');

		print_debug($this->reader->get_component($mmf));

	}

}