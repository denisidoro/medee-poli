<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index() {
		
		$data = array(
			'view' => 'home',
			'title' => 'Medee',
			'content' => array()
		);

		$this->template->default_write($data);

	}

	public function index2()
	{
		$this->load->view('welcome');
	}

	public function slider() {
		
		$data = array(
			'view' => 'test/slider',
			'title' => 'Medee',
			'content' => array()
		);

		$this->template->default_write($data);

	}

	public function test()
	{
		$this->load->library('array2xml');
		$this->load->library('reader');
		$this->load->library('generator');
	}

}