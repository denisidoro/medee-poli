<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generation extends CI_Controller {

	public function xml()
	{
	
		$this->load->library(array('array2xml', 'epfparser', 'xmlcreator'));
		$this->load->helper('form');

		$arrays = $this->xmlcreator->get_array_with_fragments();
		$xml_return = $this->xmlcreator->generate_xml($arrays);

		$data = array(
			'info' => array(
				$arrays, 
				$xml_return
			)
	 	);

		$this->load->view('xmlgen', $data);

	}

}