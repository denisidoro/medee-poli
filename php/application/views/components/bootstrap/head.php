<head>
<meta charset="utf-8">
<title><?= $title; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Le styles -->
<link href="<?= RESOURCE_ROOT; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= RESOURCE_ROOT; ?>bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

<script src="<?= RESOURCE_ROOT; ?>jquery/jquery-1.9.1.min.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/minified/jquery-ui.min.js"></script>
<!--<script src="<?= RESOURCE_ROOT; ?>bootstrap/js/jquery.min.js"></script>-->
<script src="<?= RESOURCE_ROOT; ?>bootstrap/js/bootstrap.min.js"></script> 

<!--<script src="<?= RESOURCE_ROOT; ?>bootstrap/js/spin.min.js"></script>-->
<script src="<?= RESOURCE_ROOT; ?>bootstrap/js/bootstrap-ajax.js"></script>

<!--
<script src="<?= RESOURCE_ROOT; ?>messenger/js/messenger.min.js"></script>
<script src="<?= RESOURCE_ROOT; ?>messenger/js/messenger-theme-future.js"></script>
<link href="<?= RESOURCE_ROOT; ?>messenger/css/messenger.css" rel="stylesheet">
<link href="<?= RESOURCE_ROOT; ?>messenger/css/messenger-theme-future.css" rel="stylesheet">
-->

<style type="text/css">
  body {
  }
  .sidebar-nav {
    padding: 9px 0;
  }
  .bs-docs-separator {
    margin-top: 50px;
  }
  .page-header {
    margin: 10px 0 10px;
  }
  @media (max-width: 980px) {
    body {
      padding-top: 0px;
    }
    /* Enable use of floated navbar text */
    .navbar-text.pull-right {
      float: none;
      padding-left: 5px;
      padding-right: 5px;
    }
  }
  @media (min-width: 820px) {
    .sidebar-nav-fixed {
        position: fixed;
        width:23.404%;
    }      
  }

</style>

<style>

.hero-unit-custom {
  background: #080028;
  background: -moz-linear-gradient(left,  #080028 0%, #636270 39%, #514e59 60%, #00253a 100%);
  background: -webkit-linear-gradient(left,  #080028 0%,#636270 39%,#514e59 60%,#00253a 100%);
  background: -o-linear-gradient(left,  #080028 0%,#636270 39%,#514e59 60%,#00253a 100%);
  background: -ms-linear-gradient(left,  #080028 0%,#636270 39%,#514e59 60%,#00253a 100%);
  background: linear-gradient(to right,  #080028 0%,#636270 39%,#514e59 60%,#00253a 100%);
  color: white;
}

.row-fluid > .span-fixed-sidebar {
    margin-left: 290px;
}

</style>   

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
<![endif]-->

<!-- Fav and touch icons -->
<link rel="shortcut icon" href="<?= RESOURCE_ROOT; ?>img/favicon/1_Desktop_Icons/icon_032.png">

<script>

function showReports(data) {

    var reports, type;

    if (data.reports)
      reports = data.reports;
    else if (typeof data == "string")
      reports = [data];
    
    type = (data.type) ? data.type : "success";

    reports.forEach(function (e, i, a) {
        Messenger().post({
            message: e, 
            hideAfter: 10,
            type: type,
            showCloseButton: true
        });
    });    
    
}

</script>

</head>