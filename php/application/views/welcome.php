<html>

<head>

	<title>IC :: Homepage</title>
	<link rel="stylesheet" href="<?= SERVER_LOCATION; ?>resource/css/main.css" type="text/css"/>

</head>

<body>

	<h1>IC Medee</h1>

	<p>
		<a href='reading'>Programa principal</a><br>
		<a href='generation/xml' target='_blank'>Geração do XML</a> (pode levar até 30s para carregar)<br>
		<a href='explorer.php?dir=medee_framework'>Visualização dos XMLs</a><br>
	</p>
	<p>
		<a href='docs'>Documentação do projeto</a><br>
		<a href='medee_docs/index.htm'>Documentação do Medee</a><br>
		<a href='documentation/uml/php/index.htm'>UML Report</a><br>
	</p>
	<p>
		<a href='https://github.com/denisidoro/medee-poli'>Repositório no Github</a><br>
		<a href='explorer.php'>File explorer</a>
	</p>

	<hr>
	<p>
		Melhor visualizado no <a href='https://www.google.com/intl/en/chrome/browser/'>Google Chrome</a>
	</p>

	<p>
		Environment: <?= ENVIRONMENT; ?><br>
		HTTP Host: <?= $_SERVER['HTTP_HOST']; ?>
	</p>

</body>

</html>