<html>

<head>

	<title>Programa principal</title>
	<script src="<?= RESOURCE_ROOT; ?>jquery/jquery.js"></script>
	<script src="<?= RESOURCE_ROOT; ?>jquery/regex.js"></script>
	<link rel="stylesheet" href="<?= RESOURCE_ROOT; ?>/css/main.css" type="text/css"/>

	<script>

	function updateAccordions(results) {

		console.log('update');

		results.forEach(function(e, i, a) {

			console.log(e[2]);

		});

	}

	function call_ajax(url, type, data) 
	{
		
		if (!data)
			data = [];

		console.log('ajax: ' + url + ', ' + type + ', ' + data);

		$.ajax({
		    url: url,
		    type: type,
		    async: true,
		    data: data,
		    cache: false,
		    timeout: 30000,
		    error: function(){
		        return false;
		    },
		    success: function(msg){ 
		    	console.log(1);
		    	updateAccordions(JSON.stringfy(msg));
		    }
		});

		return true;

	};

	var data = checkedBoxes = [];

	$(document).ready(function(){

		$(":checkbox").change(function(){
	    	
	    	checkedBoxes = [];
	    	weights = [];

	    	var explode = $(this).attr('value').split('_');
	    	var checked = ($(this).attr('checked')) ? true : false;

	    	if (explode[1] == 0)
	    		$(':regex(value,^' + explode[0] + '_[0-9])').each(function() {
					$(this).attr('checked', checked);
				});	

	    	$(":input[type='number']").each(function() {
    		explode = $(this).attr('name').split('_');
    		if (typeof weights[explode[1]] == 'undefined')
    			weights[explode[1]] = [];
	    		weights[explode[1]][explode[2]] = $(this).attr('value');
	    	});

			$("input:checked").each(function() {
	    		explode = $(this).attr('value').split('_');
	    		checkedBoxes.push(explode);
			});

			console.log([checkedBoxes, weights]);

			data = {
				indexes: checkedBoxes, 
				weights: weights, 
				method: $("#methodoption").val()
			};

			call_ajax('<?= SERVER_LOCATION; ?>reading/get_result', 'GET', data);

	    });

	});

	</script>

</head>

<body>

<?

	$attributes = array('id' => 'myform');
	echo form_open('', $attributes);

	$number_data = array(
		'min' => 1,
		'max' => 4,
		'step' => 1,
		'value' => 1
	);

	for ($i = 0; $i < count($menu_elements); $i++)
		if (count($menu_elements[$i]) > 1)
			for ($j = 0; $j < count($menu_elements[$i]); $j++)
			{

				$attributes = array(
				    'name'        => "factors_{$i}_{$j}",
				    'id'          => "factors_{$i}_{$j}",
				    'value'       => "{$i}_{$j}",
				    'checked'     => FALSE
				);

				if ($j > 0)
					$attributes['style'] = 'margin-left: 25px';

				echo form_checkbox($attributes);
				echo form_label($menu_elements[$i][$j]['presentationName'], $attributes['id']);

				if ($j > 0)
				{
					$number_data['name'] = $number_data['id'] = "number_{$i}_{$j}";
					echo form_input($number_data, '', '', 'number');
				}

				echo '</br>';

			}

	$options = array(
		'get_weights',
		'setup_initial_weights',
		'get_fragments_from_factors'
	);

	echo "<p><h3>METHOD</h3>" . form_dropdown('methodoption', $options, 0, "id='methodoption'") . "</p>";

	echo form_close();

	$attributes = array(
		'value' => '', 
		'id' => 'frags',
		'name' => 'frags'
	);
	echo "<p><h3>RESULT</h3>" . form_textarea($attributes) . "</p>";

?>

</body>

</html>