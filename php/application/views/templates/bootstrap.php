<!DOCTYPE html>
<html lang="en">

  <?= $head; ?>

  <body data-spy="scroll" data-target="#navbarExample">

    <?= $header; ?>

    <?= $content; ?>

    <div class="container-fluid">
      <?= $footer; ?>
    </div>

  </body>
</html>
