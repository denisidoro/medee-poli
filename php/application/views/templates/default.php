<!DOCTYPE html>
<html>

	<?= $head; ?>

	<body>

		<div data-role="page">

			<?= $header; ?>

			<div class='height-min'> 
				<?= $content; ?>  
			</div>
			
			<?= $footer; ?>  
			
		</div>
	
	</body>

</html>