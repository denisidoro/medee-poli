<link rel="stylesheet" href="<?= RESOURCE_ROOT; ?>jquery-ui/themes/base/jquery.ui.all.css">
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/jquery-1.9.1.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.core.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.widget.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.mouse.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.slider.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery/regex.js"></script>

<script>

var latestMod = new Date().getTime();

function call_ajax(url, type, data) 
{
	
	if (!data)
		data = [];

	console.log('ajax: ' + url + ', ' + type + ', ' + data);

	$.ajax({
	    url: url,
	    type: type,
	    async: true,
	    data: data,
	    cache: false,
	    timeout: 30000,
	    error: function(){
	        return false;
	    },
	    success: function(msg){ 
	    	updateAccordions(JSON.stringify(msg));
	        return true;
	    }
	});

	return true;

};

function updateAccordions(results) {

	results.forEach(function(e, i, a) {

		console.log(e[2]);

	});

}

$(document).ready(function(){
});

function updateAccordions(results) {

	console.log('update');

	results.forEach(function(e, i, a) {

		console.log(e[2]);

	});

}

function getResults() {

	var currDate = new Date().getTime();

	console.log([currDate, latestMod])

	if (currDate - latestMod < 500) {
		return false;
	}

	latestMod = currDate;

	var checkedBoxes = [];
	var weights = [];		 

	$(':regex(id,^slider-[0-9]-[0-9])').each(function() {

		var id = $(this).attr('id').split('-');		
		var val = $(this).slider('value');

		if (val > 0) 
			checkedBoxes.push([id[1], id[2]]);

		if (typeof weights[id[1]] == 'undefined')
			weights[id[1]] = [];

		weights[id[1]][id[2]] = val;

	});	

	var data = {
		indexes: checkedBoxes, 
		weights: weights, 
		method: 0
	};

	//console.log(data);
	call_ajax('<?= SERVER_LOCATION; ?>reading/get_result', 'GET', data);

}

function onSliderChange() {

	var slider = $(this);
	var id = slider.attr('id');

	if (id.split('-').length == 2) {

		$(':regex(id,^' + id + '-[0-9])').each(function() {
			$(this).slider('value', slider.slider('value'));
		});	

	}

	getResults();

}

$(function() {
	$(".slider").slider({
		orientation: "horizontal",
		range: "min",
		max: 5,
		min: 0,
		value: 0,
		step: 1,
		change: onSliderChange
	});
});

</script>

<style>

.slider {
	display: inline-block;
	float: right;
	margin-left: 40px;
	width: 200px;
}

.factor {
	padding-top: 20px 0;
}

.accordion-toggle.red {
	background: red;
}

</style>
 


<div class="accordion" id="accordion-factors"></div>

<?php

$i = 0;

foreach ($menu_elements as $fgroup) {

	echo "<div class='accordion-group'><div class='accordion-heading'><a id='data-toggle-$i' class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#collapse-$i'>";
	echo $fgroup[0]['presentationName'];
	echo "<div id='slider-$i' class='slider'></div>";
	echo "</a></div>";

	echo "<div id='collapse-$i' class='accordion-body collapse'>";
	echo "<div class='accordion-inner'>";

	array_shift($fgroup);
	$j = 1;

	foreach ($fgroup as $f) {
		echo "<div class='factor'>";
		echo $f['presentationName'];
		echo "<div id='slider-$i-" . $j++ . "' class='slider'></div>";
		echo "</div>";
	}

	echo "</div>";
	echo "</div>";


	echo "</div>";

	$i++;

}

?>


<!--
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-factors" href="#collapseOne">
        Collapsible Group Item #1
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse in">
      <div class="accordion-inner">
        Anim pariatur cliche...
      </div>
    </div>
  </div>
  
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-factors" href="#collapseTwo">
        Collapsible Group Item #2
      </a>
    </div>
    <div id="collapseTwo" class="accordion-body collapse">
      <div class="accordion-inner">
        <div class="progress progress-striped">
          <div class="bar" style="width: 20%;">asdf</div>
        </div>
      </div>
    </div>
  </div>

</div>	
-->