<link rel="stylesheet" href="<?= RESOURCE_ROOT; ?>jquery-ui/themes/base/jquery.ui.all.css">
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/jquery-1.9.1.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.core.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.widget.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.mouse.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.slider.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery/regex.js"></script>


<style>

body, html {
  background: #020031; /* Old browsers */
  background: -moz-linear-gradient(45deg,  #020031 0%, #6d3353 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,#020031), color-stop(100%,#6d3353)); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(45deg,  #020031 0%,#6d3353 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(45deg,  #020031 0%,#6d3353 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(45deg,  #020031 0%,#6d3353 100%); /* IE10+ */
  background: linear-gradient(45deg,  #020031 0%,#6d3353 100%); /* W3C */
}

.mytextarea{
  width: 100%;
  height: 150px;
}

.accordion-inner, .accordion-heading {
background: #f7fbfc; /* Old browsers */
background: -moz-linear-gradient(top, #f7fbfc 0%, #d9edf2 40%, #add9e4 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f7fbfc), color-stop(40%,#d9edf2), color-stop(100%,#add9e4)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, #f7fbfc 0%,#d9edf2 40%,#add9e4 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, #f7fbfc 0%,#d9edf2 40%,#add9e4 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, #f7fbfc 0%,#d9edf2 40%,#add9e4 100%); /* IE10+ */
background: linear-gradient(to bottom, #f7fbfc 0%,#d9edf2 40%,#add9e4 100%); /* W3C */

}

</style>

<script>

var latestMod = new Date().getTime();

function call_ajax(url, type, data) 
{
  
  if (!data)
    data = [];

  console.log('ajax: ' + url + ', ' + type + ', ' + data);

  $.ajax({
      url: url,
      type: type,
      async: true,
      data: data,
      cache: false,
      timeout: 30000,
      error: function(){
          return false;
      },
      success: function(msg){ 
        updateAccordions(JSON.parse(msg));
      }
  });

  return true;

};

$(document).on('click', ':checkbox', function(){

  var text = '';

  $("input:checked").each(function() {
    text += $(this).attr('value') + '\n';
    
  });

  $('#selected_mmfs').val(text);

});


function clearResults() {
  $('#collapse-requirements .accordion-inner').html('');
  $('#collapse-analysis .accordion-inner').html('');
  $('#collapse-test .accordion-inner').html('');
  $('#collapse-discipline .accordion-inner').html('');
  $('#collapse-design .accordion-inner').html('');
  $('#collapse-agent .accordion-inner').html('');
  $('#collapse-environment .accordion-inner').html('');
  $('#collapse-interaction .accordion-inner').html('');
  $('#collapse-organization .accordion-inner').html('');
}

function updateAccordions(results) {

  console.log(results);

  clearResults();

  results.forEach(function(e, i, a) {

    var d = e[2].split('_');
    var c = e[3].split('_');

    $('#collapse-'+d[0]+' .accordion-inner').append('<li><input class="checkbox" type="checkbox" name="box-discipline" value="'+e[4]+'"> '+e[4]+'</input>, '+e[1]+'</li>');
    $('#collapse-'+c[0]+' .accordion-inner').append('<li><input class="checkbox" type="checkbox" name="box-component" value="'+e[4]+'"> '+e[4]+'</input>, '+e[1]+'</li>');

  });

}

function getResults() {

  var currDate = new Date().getTime();

  //console.log([currDate, latestMod])

  //if (currDate - latestMod < 500) {
    //return false;
  //}

  latestMod = currDate;

  var checkedBoxes = [];
  var weights = [];    

  $(':regex(id,^slider-[0-9]-[0-9])').each(function() {

    var id = $(this).attr('id').split('-');   
    var val = $(this).slider('value');

    if (val > 0) 
      checkedBoxes.push([id[1], id[2]]);

    if (typeof weights[id[1]] == 'undefined')
      weights[id[1]] = [];

    weights[id[1]][id[2]] = val;

  }); 

  var data = {
    indexes: checkedBoxes, 
    weights: weights, 
    method: 0
  };

  console.log('checkedBoxes');
  console.log(checkedBoxes);

  if (checkedBoxes.length == 0) {
    clearResults();
    return;
  }

  //console.log(data);
  call_ajax('<?= SERVER_LOCATION; ?>reading/get_result', 'GET', data);

}

function onSliderChange() {

  var slider = $(this);
  var id = slider.attr('id');

  if (id.split('-').length == 2) {

    $(':regex(id,^' + id + '-[0-9])').each(function() {
      $(this).slider('value', slider.slider('value'));
    }); 

  }

  getResults();

}

$(function() {
  $(".slider").slider({
    orientation: "horizontal",
    range: "min",
    max: 5,
    min: 0,
    value: 0,
    step: 1,
    change: onSliderChange
  });
});

</script>

<style>

.slider {
  display: inline-block;
  float: right;
  margin-left: 40px;
  width: 200px;
}

.factor {
  padding-top: 20px 0;
}

.accordion-toggle.red {
  background: red;
}

</style>
 

<div class='background-holder' style='padding:0px 240px; margin:25px;'>

  <div class="accordion" id="accordion-factors"></div>

    <?php

    $i = 0;

    foreach ($menu_elements as $fgroup) {

      echo "<div class='accordion-group'><div class='accordion-heading'><a id='data-toggle-$i' class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#collapse-$i'>";
      echo $fgroup[0]['presentationName'];
      echo "<div id='slider-$i' class='slider'></div>";
      echo "</a></div>";

      echo "<div id='collapse-$i' class='accordion-body collapse'>";
      echo "<div class='accordion-inner'>";

      array_shift($fgroup);
      $j = 1;

      foreach ($fgroup as $f) {
        echo "<div class='factor'>";
        echo $f['presentationName'];
        echo "<div id='slider-$i-" . $j++ . "' class='slider'></div>";
        echo "</div>";
      }

      echo "</div>";
      echo "</div>";


      echo "</div>";

      $i++;

    }

    ?>

    <hr>

    <p id="discipline-holder">

      <div class="accordion-discipline" id="accordion">

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-discipline" href="#collapse-requirements">
              Requirements
            </a>
          </div>

          <div id="collapse-requirements" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>
          </div>

        </div>

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-discipline" href="#collapse-analysis">
              Analysis
            </a>
          </div>

          <div id="collapse-analysis" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>

        </div>

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-discipline" href="#collapse-design">
              Design
            </a>
          </div>

          <div id="collapse-design" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>
          </div>

        </div>

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-discipline" href="#collapse-implementation">
              Implementation
            </a>
          </div>

          <div id="collapse-implementation" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>
          </div>

        </div>

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-discipline" href="#collapse-test">
              Test
            </a>
          </div>

          <div id="collapse-test" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>
          </div>

        </div>

      </div>

    </p>

    <hr>


    <p id="component-holder">

      <div class="accordion" id="accordion">

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion-component" href="#collapse-agent">
              Agent
            </a>
          </div>

          <div id="collapse-agent" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>
          </div>

        </div>

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion-component" href="#collapse-environment">
              Environment
            </a>
          </div>

          <div id="collapse-environment" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>

        </div>

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion-component" href="#collapse-interaction">
              Interaction
            </a>
          </div>

          <div id="collapse-interaction" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>
          </div>

        </div>

        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#acordion-component" href="#collapse-organization">
              Organization
            </a>
          </div>

          <div id="collapse-organization" class="accordion-body collapse">
            <div class="accordion-inner">
            </div>
          </div>

        </div>

      </div>

    </p>

    <hr>

    <p>
      <textarea class='mytextarea' name="selected_mmfs" id="selected_mmfs"></textarea>
    </p>

</div>