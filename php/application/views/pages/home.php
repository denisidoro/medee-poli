<link href="<?= RESOURCE_ROOT; ?>css/home.css" rel="stylesheet">

<div class="jumbotron masthead">
  <div class="container">
    <h1>Medee</h1>
    <p>Medee is a framework for composing MAS situational methods out of method fragments according to a given project situation, by applying the principles proposed by Situational Method Engineering. It offers a method fragment repository that covers different development phases, like requirements, analysis, design, implementation, as well as the main components of a MAS application, such as agents, environments, interactions, and organizations. Such a repository has been sourced from several MAS development approaches, as such Gaia, Tropos, Ingenias, PASSI, MOISE, and OperA.</p>
    <p>
      <a href="reading/temp" class="btn btn-primary btn-large">Suggest fragments</a>
    </p>
    <ul class="masthead-links">
      <?php $this->load->view("components/bootstrap/links"); ?>
    </ul>
  </div>
</div>
