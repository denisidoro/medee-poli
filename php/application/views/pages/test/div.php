<link rel="stylesheet" href="<?= RESOURCE_ROOT; ?>jquery-ui/themes/base/jquery.ui.all.css">
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/jquery-1.9.1.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.core.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.widget.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.mouse.js"></script>
<script src="<?= RESOURCE_ROOT; ?>jquery-ui/ui/jquery.ui.slider.js"></script>


<style>

.factor {
	width: 230px;
	height: 50px;
	margin: 10px;
	padding: 20px;
	border-radius: 10px;
	box-shadow: 2px;
}

.factor.red {
	color: white;
	background: red;
}

.factor.blue {
	color: white;
	background: blue;
}

.factor .slider {
	margin-top: 10px;
}

</style>


<script>

function hexFromRGB(r, g, b) {
	var hex = [
		r.toString( 16 ),
		g.toString( 16 ),
		b.toString( 16 )
	];
	$.each( hex, function( nr, val ) {
		if ( val.length === 1 ) {
			hex[ nr ] = "0" + val;
		}
	});
	return hex.join( "" ).toUpperCase();
}

function updateOpacity() {

	var id = $(this).attr('id').split('-');
	id = id[1];

	$("#sliderholder-" + id).css('background-color', 'rgba(255,0,0,' + $(this).slider('value') + ')');

}

function preg_match_all(regex, haystack) {
   var globalRegex = new RegExp(regex, 'g');
   var globalMatch = haystack.match(globalRegex);
   matchArray = new Array();
   for (var i in globalMatch) {
      nonGlobalRegex = new RegExp(regex);
      nonGlobalMatch = globalMatch[i].match(nonGlobalRegex);
      matchArray.push(nonGlobalMatch[1]);
   }
   return matchArray;
}

$(function() {

	$("#slider-0, #slider-1").slider({
		orientation: "horizontal",
		range: "min",
		max: 1,
		value: 1,
		step: 0.01,
		slide: updateOpacity,
		change: updateOpacity
	});

});

</script>

<div id='sliderholder-0' class='factor red'>
	<span class='name'>Project team size</span>
	<div id='slider-0' class='slider'></div>
</div>

<div id='sliderholder-1' class='factor red'>
	<span class='name'>Problem Susceptability to Change</span>
	<div id='slider-1' class='slider'></div>
</div>