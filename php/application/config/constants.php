<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|--------------------------------------------------------------------------
| MEDEE directories
|--------------------------------------------------------------------------
|
*/

if (ENVIRONMENT == 'development') {
	define('SERVER_LOCATION', 'http://localhost:8080/medee-poli/php/');
	define('SERVER_ROOT', '/medee-poli/php/');
	define('SERVER_PATH', 'C:/localhost/ic');
}
else {
	define('SERVER_LOCATION', 'https://medee-poli.rhcloud.com/');
	define('SERVER_ROOT', '/');
	define('SERVER_PATH', '');
}

define('RESOURCE_ROOT', SERVER_ROOT . 'resource/');

define('MAIN_XML_LOCATION',	'medee_framework/');

define('MEDEE_DIRECTORY', SERVER_LOCATION . 'medee_framework/medee_plugins/');
define('MEDEE_GUIDELINES_DIRECTORY', MEDEE_DIRECTORY . 'c_medee_composition_guidelies/guidances/guidelines/');

/* End of file constants.php */
/* Location: ./application/config/constants.php */