<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function exst( & $var, $default = "")
{
    $t = "";
    if ( !isset($var)  || !$var ) {
        if (isset($default) && $default != "") $t = $default;
    }
    else  {  
        $t = $var;
    }
    if (is_string($t)) $t = trim($t);
    return $t;
}

function print_debug($content, $color = NULL)
{
	echo "<pre><p";
	echo (isset($color)) ? " style='color:{$color}'>" : ">";
	print_r($content);
	echo "</p></pre>";
}	

function js_printable($array, $comma = false)
{

	$search = array('[', ']', '{', '}', '"', ',name:', 'name:', ',count:');
	$replace = array('', '', '', '', '', '\\n', '', ': ');

	if ($comma)
	{
		$search[] = ',';
		$replace[] = '\n';
	}

	$array = str_replace(
		$search,
		$replace,
		json_encode($array)
	);

	return $array;

}
