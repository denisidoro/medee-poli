<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_category_folders($directory = NULL)
{

	if ($directory == NULL)
		$directory = str_replace(SERVER_LOCATION, './', MEDEE_DIRECTORY);

	$fragment_folders = array();
	$folders = glob($directory . "*");

	foreach ($folders as $folder)
		if (stripos($folder, 'fragment') !== FALSE)
		{
			$folder_dirs = explode('/', $folder);
			$fragment_folders[] = end($folder_dirs);
		}

	return $fragment_folders;	

}